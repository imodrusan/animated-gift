![Gift](favico.png)

## Animated Gift


#### Use this to surprise someone on their birthday. 

- Customise the text and images to your preferences
- You can send html file over most social media for the other person to open or host it using some provider
- Works on mobile and desktop

#### Live preview: **https://lucky-panda-2bf638.netlify.app/**